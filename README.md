# Cross Product Visualization tool

# How to use
0. You must have python3 installed first
1. Download zip file
2. Unzip zip file
3. Open terminal (on mac hit command + spacebar and type in terminal)
4. Type `cd Downloads/cross-product-master`
5. Type `bash install.sh`
6. Wait for script to finish installing dependencies
7. Once finished, type `source myvenv/bin/activate`
8. Type `python3 plot.py` to open graph of charges
9. Open `plot.py` with a text editor to edit vectors
10. Edit the vectors in variable `store` according to the instructions in the program
11. Edit the limits to expand for bigger vectors if needed