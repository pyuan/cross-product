import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.patches as mpatches
import numpy as np

# cross product
def crossProduct(a, b):
    cx = (a[4]*b[5]) - (a[5]*b[4])
    cy = (a[3]*b[5]) - (a[5]*b[3])
    cz = (a[3]*b[4]) - (a[4]*b[3])
    ca = [0, 0, 0, cx, cy, cz]
    return ca

# dot product
def dotProduct(a, b):
    return (a[3]*b[3]) + (a[4]*b[4]) + (a[5]*b[5])

# edit these values to change the vector
# do not edit the first three zeros of each list
# for example, you can edit 1, 1, 1 in the first list inside store
# or edit 1, 4, 2 in the second list inside store
store = [[0, 0, 0, 1, 1, 1], [0, 0, 0, 1, 4, 2]]

# append cross product vector to store
store.append(crossProduct(store[0], store[1]))

# create individual vector arrays
v1 = np.array(store[0])
v2 = np.array(store[1])
v3 = np.array(store[2])

# assign variables based on vector coordinates
X, Y, Z, U, V, W = v1
A, B, C, D, E, F = v2
G, H, I, J, K, L = v3

# start plot
fig = plt.figure()

# 3d
ax = fig.add_subplot(111, projection='3d')

# plot the vector arrows
plt.quiver(X, Y, Z, U, V, W, color='b')
plt.quiver(A, B, C, D, E, F, color='r')
plt.quiver(G, H, I, J, K, L, color='g')

# compute dot product
dot = dotProduct(store[0], store[1])

# create label variables
s1 = f"({U},{V},{W})"
s2 = f"({D},{E},{F})"
s3 = f"({J},{K},{L}) magnitude:{dot}"

# create labels
ax.text(store[0][3], store[0][4], store[0][5], s1, color="black")
ax.text(store[1][3], store[1][4], store[1][5], s2, color="black")
ax.text(store[2][3], store[2][4], store[2][5], s3, color="black")

# Legend
blue_patch = mpatches.Patch(color='blue', label='Vector #1')
red_patch = mpatches.Patch(color='red', label='Vector #2')
green_patch = mpatches.Patch(color='green', label='Vector #3 (Result)')
ax.legend(handles=[blue_patch, red_patch, green_patch], loc='lower left', bbox_to_anchor=(.83, .93))

# set limits
# this can be changed to fit larger vectors
ax.set_xlim([-5, 5])
ax.set_ylim([-5, 5])
ax.set_zlim([-5, 5])

# axis names
ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')

# show plot
plt.show()
